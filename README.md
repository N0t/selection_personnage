# Selection_Personnage

## Sommaire

<!--ts-->
   * [Fonctionnalités implémentées](#fonctionnalités-implémentées)
   * [Sources exploitéess](#sources-exploitées)
   * [Récupérer le projet](#récupérer-le-projet)
<!--te-->


## Fonctionnalités implémentées

- Chargement des données de personnages depuis un fichier JSON
- Affichage de la liste des personnages (noms et caractéristiques)
- Affichage complet d'un personnage
- Ajout d'un nouveau personnage
- Suppression d'un ou plusieurs personnages existants (en les cochant dans la liste des personnages)
- Modification d'un personnage existant à partir de sa page
- Script Editor permettant de charger les données du JSON afin d'afficher la liste des noms de personnages



## Sources exploitées

### Tutoriels

- Template de .gitignore recommandé par GitHub : https://github.com/github/gitignore/blob/master/Unity.gitignore
- Tutoriel Unity de création d'un quizz exploitant un fichier JSON : https://unity3d.com/fr/learn/tutorials/topics/scripting/intro-part-two?playlist=17117
- Tutoriel Unity de mise en place d'un Scroll Rect : https://www.youtube.com/watch?v=tcU8yzv_xEw


### Forums

- Adaptatation de chemin de fichier JSON pour l'éditeur Unity et le build WebGL : https://stackoverflow.com/questions/43693213/application-streamingassetspath-and-webgl-build
- Désactivation du clic sur les enfants d'un bouton : https://www.reddit.com/r/Unity3D/comments/351yxh/how_can_i_get_a_ui_button_to_stop_treating_its/


### Ressources libres de droit

- Icone Age : https://fr.freepik.com/icones-gratuites/sables-du-temps_717611.htm#term=sablier%20icone&page=1&position=19
- Icone Force : https://www.flaticon.com/free-icon/sword_65741
- Icone Vitesse : https://www.onlinewebfonts.com/icon/556060
- Icone Poubelle : https://icons8.com/icon/362/trash-can
- Icone Ajout : https://www.iconfinder.com/icons/392530/add_create_cross_new_plus_icon
- Icone Modification : https://visualpharm.com/free-icons/create-595b40b65ba036ed117d1002
- Icone Retour : https://www.flaticon.com/free-icon/back-arrow_340



## Récupérer le projet

- Ce projet est récupérable par un simple clonage Git, ou par téléchargement en fichier ZIP.
- Dans l'éditeur Unity, le projet doit obligatoirement être lancé depuis la scène "Start".
- Le fichier JSON "characters.json" doit se trouver dans le répertoire "Assets/StreamingAssets".
- Le script Editor est utilisable depuis l'onglet "Window/Liste des noms des personnages".

