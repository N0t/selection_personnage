﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class ListeNoms : EditorWindow
{
    // Classe contenant les données sur les personnages
    public GameData gameData;
    // Liste des noms des personnages
    public List<string> listeDesNoms;
    // Nom du fichier JSON contenant les données des personnages
    private string charactersDataFileName = "characters.json";

    // Use this for initialization
    [MenuItem ("Window/Liste des noms de personnages")]
    static void Init ()
    {
        ListeNoms window = (ListeNoms)EditorWindow.GetWindow(typeof(ListeNoms));
        window.Show();
	}


    private void OnGUI()
    {
        // Affichage des données si elles ont été chargées
        if (gameData != null)
        {

            // Affichage dans la fenêtre de la liste des noms
            SerializedObject serializedObject = new SerializedObject(this);
            SerializedProperty serializedProperty = serializedObject.FindProperty("listeDesNoms");
            EditorGUILayout.PropertyField(serializedProperty, true);
            serializedObject.ApplyModifiedProperties();
        }
        
        // Bouton permettant de charger la liste des personnages à partir du JSON
        if (GUILayout.Button("Load data"))
        {
            LoadGameData();
        }
    }


    // Chargement du JSON
    protected void LoadGameData()
    {
        // Récupération du chemin complet du JSON
        string filePath = Path.Combine(Application.streamingAssetsPath, this.charactersDataFileName);

        // Si le fichier existe
        if (File.Exists(filePath))
        {
            // Récupération en brut du contenu du fichier
            string dataAsJson = File.ReadAllText(filePath);
            
            // Conversion du contenu brut en classe
            gameData = JsonUtility.FromJson<GameData>(dataAsJson);

            // Initialisation de la liste des noms
            listeDesNoms = new List<string>();
            // Récupération de la liste des noms à partir du gameData
            for (int i = 0; i < gameData.characters.Length; i++)
            {
                listeDesNoms.Add(gameData.characters[i].name);
            }
        }
        // Sinon, affichage d'une erreur en console (alerte de debug)
        else
        {
            Debug.Log("JSON non trouvé à l'addresse : " + filePath);
        }
    }
}
