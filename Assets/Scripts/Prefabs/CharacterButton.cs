﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterButton : MonoBehaviour
{
    // Texte des caractéristiques à afficher
    public GameObject texteNom;
    public GameObject texteAge;
    public GameObject texteForce;
    public GameObject texteVitesse;



    // Fonction de redirection vers la page du personnage associé
    public void toCharacterPage()
    {
        // Modification de l'index du personnage stocké dans le DataController
        GameObject.FindWithTag("DataController").GetComponent<DataController>().selectedCharacter
            = GameObject.FindWithTag("ListController").GetComponent<ListController>().characters.IndexOf(this.gameObject);

        // Charcement de la page du personnage
        SceneManager.LoadScene("CharacterDetail");
    }


    // Déclenché quand la case du couton est cochée
    public void onClickToggle()
    {
        GameObject.FindWithTag("ListController").GetComponent<ListController>().updateToggle(this.gameObject);
    }


    // Fonction de mise à jour de l'affichage des caractéristiques du personnage
    public void updateInfos(string nom, int age, float force, float vitesse)
    {
        // Vérification de l'existence du nom
        if (nom == null)
        {
            nom = "";
        }
        this.texteNom.GetComponent<Text>().text = nom;
        
        // Si les attributs int/float valent 0, on suppose qu'ils sont inexistants dans le JSON
        // Les attributs inexistants ne sont pas affichés
        if (age == 0)
        {
            this.texteAge.GetComponent<Text>().text = "";
        }
        else
        {
            this.texteAge.GetComponent<Text>().text = age.ToString();
        }
        if (force == 0)
        {
            this.texteForce.GetComponent<Text>().text = "";
        }
        else
        {
            this.texteForce.GetComponent<Text>().text = force.ToString();
        }
        if (vitesse == 0)
        {
            this.texteVitesse.GetComponent<Text>().text = "";
        }
        else
        {
            this.texteVitesse.GetComponent<Text>().text = vitesse.ToString();
        }
    }
}
