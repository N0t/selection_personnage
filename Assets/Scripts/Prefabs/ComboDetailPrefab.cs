﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboDetailPrefab : MonoBehaviour
{
    // Texte du nom du  combo
    public GameObject texteNom;
    // Texte des mouvements du combo
    public GameObject texteMouvements;



    // Fonction de mise à jour de l'affichage des caractéristiques du personnage
    public void updateInfos(string nom, string[] mouvements)
    {
        // Vérification de l'existence du nom
        if (nom == null)
        {
            nom = "";
        }
        this.texteNom.GetComponent<Text>().text = nom;

        // Vérification de la présence de mouvements
        if (mouvements.Length != 0)
        {
            // Premier mouvements
            this.texteMouvements.GetComponent<Text>().text = mouvements[0];

            for (int i = 1; i < mouvements.Length; i++)
            {
                // Mouvements suivants avec séparateur
                this.texteMouvements.GetComponent<Text>().text += ", " + mouvements[i];
            }
        }
    }


    // Fonction appelée lors du clic sur le bouton Supprimer Combo
    public void onClickSupprimerCombo()
    {
        GameObject.FindWithTag("ListController").GetComponent<AjouterController>().supprimerCombo(this.gameObject);
    }
}
