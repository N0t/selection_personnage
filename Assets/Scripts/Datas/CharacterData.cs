﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Classe correspondant à un personnage unique
[System.Serializable]
public class CharacterData
{
    public string name;
    public int age;
    public float strength;
    public float speed;
    public ComboData[] combo;
}
