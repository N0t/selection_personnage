﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Classe utilisée pour récupérer le contenu du JSON sous la forme d'une liste de personnages
[System.Serializable]
public class GameData
{
    public CharacterData[] characters;
}
