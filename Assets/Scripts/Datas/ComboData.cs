﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Classe correspondant à un combo de personnage
[System.Serializable]
public class ComboData
{
    public string name;
    public string[] movements;
}
