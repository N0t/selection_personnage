﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Linq;

public class DataController : MonoBehaviour
{
    // Nom du fichier JSON contenant les données des personnages
    private string charactersDataFileName = "characters.json";
    // Liste des personnages
    public List<CharacterData> characters;
    // Personnage actuellement sélectionné (utilisé pour l'affichage détaillé et la modification)
    public int selectedCharacter;


    // Use this for initialization
    protected void Start ()
    {
        // Initialisation de la liste
        this.characters = new List<CharacterData>();

        // Persistance du DataController
        DontDestroyOnLoad(this.gameObject);
        // Chargement du JSON
        LoadGameData();
        
        // Chargement de la scène initiale : liste des personnages
        SceneManager.LoadScene("Liste");
	}


    // Chargement du JSON
    protected void LoadGameData()
    {
        // Récupération du chemin complet du JSON
        string filePath = Path.Combine(Application.streamingAssetsPath, this.charactersDataFileName);
        
        // Si le fichier existe
        if (File.Exists (filePath))
        {
            // Récupération en brut du contenu du fichier
            string dataAsJson = File.ReadAllText(filePath);
            // Conversion du contenu brut en classe
            GameData loadedData = JsonUtility.FromJson<GameData>(dataAsJson);

            // Initialisation de la liste des personnages à partir de la liste de CharacterData ainsi créée
            characters = loadedData.characters.ToList<CharacterData>();
        }
        // Sinon, affichage d'une erreur en console (alerte de debug)
        else
        {
            Debug.Log("JSON non trouvé à l'addresse : " + filePath);
        }
    }

    // Insertion d'un personnage dans le JSON
    public void addCharacter(CharacterData character)
    {
        // Ajout du personnage à la fin de la liste
        this.characters.Add(character);

        // Mise à jour du JSON
        this.updateJson();
    }

    // Modification d'un personnage dans le JSON
    public void updateCharacter(CharacterData character)
    {
        // Suppression de l'ancienne version du personnage
        this.characters.RemoveAt(selectedCharacter);
        
        // Insertion de sa nouvelle version au même index
        this.characters.Insert(selectedCharacter, character);

        // Mise à jour du JSON
        this.updateJson();
    }

    // Suppression de personnages dans le JSON
    public void removeCharacters(List<int> indexesToDelete)
    {
        // Tri des index (croissant)
        indexesToDelete.Sort();

        // Parcours décroissant des index
        for (int i = indexesToDelete.Count-1 ; i >= 0; i--)
        {
            // Suppression du personnage à l'index le plus éloigné dans la liste
            this.characters.RemoveAt(indexesToDelete[i]);
        }

        // Mise à jour du JSON
        this.updateJson();
    }

    protected void updateJson()
    {
        // Création du GameData pour la conversion en JSON
        GameData dataToJson = new GameData();
        dataToJson.characters = this.characters.ToArray();

        // Conversion en JSON brut
        string dataAsJson = JsonUtility.ToJson(dataToJson);

        // Sauvegarde
        File.WriteAllText(Path.Combine(Application.streamingAssetsPath, "characters.json"), dataAsJson);
    }
}
