﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class ModifierController : AjouterSupprimerHyperclass
{
    // Le DataController
    protected DataController dataController;

    // Caractéristiques à récupérer
    public GameObject texteNom;
    public GameObject texteAge;
    public GameObject texteForce;
    public GameObject texteVitesse;

    public GameObject contentCombos;

    // Prefab combo
    public GameObject prefabCombo;
    // Hauteur d'un prefab combo
    public float hauteurPrefabCombo;
    // Largeur d'un prefab combo
    public float largeurPrefabCombo;



    // Use this for initialization
    void Start()
    {
        // Récupération du DataController
        this.dataController = GameObject.FindWithTag("DataController").GetComponent<DataController>();

        // Complétion des champs avec les valeurs existantes 
        this.fillDatas();
    }


    // Fonction appelée lors du clic sur le bouton Annuler
    public void onClickAnnuler()
    {
        // Chargement du détail du personnage
        SceneManager.LoadScene("CharacterDetail");
    }


    // Fonction appelée lors du clic sur le bouton Valider
    public void onClickValider()
    {
        // Construction du nouveau personnage
        CharacterData newCharacter = new CharacterData();

        // Ajout des caractéristiques, passage des caractérisques numériques vides à 0
        newCharacter.name = texteNom.GetComponent<InputField>().text;

        string strAge = texteAge.GetComponent<InputField>().text;
        if (strAge != "")
        {
            if (strAge[0] != '.')
            {
                newCharacter.age = Convert.ToInt32(strAge);
            }
        }
        else
        {
            newCharacter.age = 0;
        }

        string strStrength = texteForce.GetComponent<InputField>().text;
        if (strStrength != "")
        {
            if (strStrength[0] != '.')
            {
                newCharacter.strength = float.Parse(strStrength);
            }
        }
        else
        {
            newCharacter.strength = 0;
        }

        string strSpeed = texteVitesse.GetComponent<InputField>().text;
        if (strSpeed != "")
        {
            if (strSpeed[0] != '.')
            {
                newCharacter.speed = float.Parse(strSpeed);
            }
        }
        else
        {
            newCharacter.speed = 0;
        }

        // Ajout des combos
        List<ComboData> newCombos = new List<ComboData>();
        for (int i = 0; i < contentCombos.transform.childCount - 1; i++)
        {
            // Création de l'élément
            ComboData newCombo = new ComboData();
            // Ajout du nom
            newCombo.name = contentCombos.transform.GetChild(i).GetComponent<ComboFieldPrefab>().texteNom.GetComponent<InputField>().text;
            // Séparation du champ Mouvements en une liste de string sur ", ", et ajout à l'élément
            newCombo.movements = contentCombos.transform.GetChild(i).GetComponent<ComboFieldPrefab>().texteMouvements.GetComponent<InputField>().text.Split(new string[] { ", " }, StringSplitOptions.None);
            // Ajout du combo à la liste
            newCombos.Add(newCombo);
        }
        // Insertion des combos dans le nouveau personnage
        newCharacter.combo = newCombos.ToArray();

        // Envoi de l'objet au DataController
        dataController.updateCharacter(newCharacter);
        
        // Redirection vers la page du personnage (pas de changement d'index à faire pour selectedController)
        SceneManager.LoadScene("CharacterDetail");
    }


    // Fonction appelée lors du clic sur le bouton Ajouter Combo
    public void onClickAjouterCombo()
    {
        // Création du prefab d'un nouveau combo
        GameObject newCombo = Instantiate(prefabCombo, Vector3.zero, Quaternion.identity);
        // Attachement du combo à l'objet Content
        newCombo.transform.SetParent(GameObject.FindWithTag("ListContent").transform, false);
        // Passage à l'avant-dernier index de Content
        newCombo.transform.SetSiblingIndex(newCombo.transform.GetSiblingIndex() - 1);

        // Redimentionnement de l'objet Content en fonction de son contenu
        contentCombos.GetComponent<RectTransform>().sizeDelta = new Vector2
            (
            50 + largeurPrefabCombo,
            80 + (hauteurPrefabCombo + 20) * (contentCombos.transform.childCount - 1)
            );
    }


    // Fonction appelée lors du clic sur le bouton Supprimer Combo
    public override void supprimerCombo(GameObject combo)
    {
        // Redimentionnement de l'objet Content en fonction de son contenu
        contentCombos.GetComponent<RectTransform>().sizeDelta = new Vector2
            (
            50 + largeurPrefabCombo,
            80 + (hauteurPrefabCombo + 20) * (contentCombos.transform.childCount - 2)
            );

        // Destruction du combo
        Destroy(combo.gameObject);
    }


    // Fonction de complétion des champs avec les valeurs existantes
    protected void fillDatas()
    {
        // Récupération du personnage sélectionné
        CharacterData selectedCharacter 
            = GameObject.FindWithTag("DataController").GetComponent<DataController>().characters[
                GameObject.FindWithTag("DataController").GetComponent<DataController>().selectedCharacter
                ];

        // Complétion des caractéristiques

        texteNom.GetComponent<InputField>().text = selectedCharacter.name;

        if (selectedCharacter.age != 0)
        {
            texteAge.GetComponent<InputField>().text = selectedCharacter.age.ToString();
        }
        if (selectedCharacter.strength != 0)
        {
            texteForce.GetComponent<InputField>().text = selectedCharacter.strength.ToString();
        }
        if (selectedCharacter.speed != 0)
        {
            texteVitesse.GetComponent<InputField>().text = selectedCharacter.speed.ToString();
        }

        // Complétion de la liste des combos
        for (int i = 0; i < selectedCharacter.combo.Length; i++)
        {
            addCombo(selectedCharacter.combo[i]);
        }
    }
    
    
    // Création et affichage du prefab d'un combo existant
    public void addCombo(ComboData combo)
    {
        // Création du prefab
        GameObject newCombo = Instantiate(prefabCombo, Vector3.zero, Quaternion.identity);
        // Attachement du combo à l'objet Content
        newCombo.transform.SetParent(GameObject.FindWithTag("ListContent").transform, false);
        // Passage à l'avant-dernier index de Content
        newCombo.transform.SetSiblingIndex(newCombo.transform.GetSiblingIndex() - 1);
        // Mise à jour du texte du bouton pour correspondre au nom du personnage
        newCombo.GetComponentInChildren<ComboFieldPrefab>().updateInfos(combo.name, combo.movements);

        // Redimentionnement de l'objet Content en fonction de son contenu
        contentCombos.GetComponent<RectTransform>().sizeDelta = new Vector2
            (
            50 + largeurPrefabCombo,
            80 + (hauteurPrefabCombo + 20) * (contentCombos.transform.childCount - 1)
            );

        // Complétion du nom du combo
        newCombo.GetComponent<ComboFieldPrefab>().updateInfos(combo.name, combo.movements);
    }
}
