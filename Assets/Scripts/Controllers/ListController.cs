﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ListController : MonoBehaviour
{
    // Liste des boutons associés aux personnages
    public List<GameObject> characters;
    // Modèle de bouton
    public Button button;
    // Largeur d'un bouton
    public float buttonWidth;
    // Le DataController
    protected DataController dataController;

    // Liste des index des éléments à supprimer
    protected List<int> indexesToDelete;
        


    // Use this for initialization
    protected void Start()
    {
        // Récupération du DataController
        this.dataController = GameObject.FindWithTag("DataController").GetComponent<DataController>();

        // Initialisation des listes
        this.characters = new List<GameObject>();
        this.indexesToDelete = new List<int>();

        // Ajout d'une case pour chaque personnage
        for (int i=0; i<dataController.characters.Count ; i++)
        {
            this.addCharacter(dataController.characters[i]);
        }
    }


    // Fonction appelée lors du clic sur le bouton Supprimer
    public void onClickSupprimer()
    {
        // Suppression des personnages dans le DataController
        this.dataController.removeCharacters(this.indexesToDelete);
        // Rechargement de la page
        SceneManager.LoadScene("Liste");
    }


    // Fonction appelée lors du clic sur le bouton Ajouter
    public void onClickAjouter()
    {
        // Chargement de la page Ajouter
        SceneManager.LoadScene("Ajouter");
    }


    // Fonction appelée lors du clic sur une case à cocher
    public void updateToggle(GameObject button)
    {
        // Si la case vient d'être cochée
        if (button.GetComponentInChildren<Toggle>().isOn)
        {
            // On ajoute l'index à la liste à supprimer
            this.indexesToDelete.Add(button.transform.GetSiblingIndex());
        }
        // Sinon
        else
        {
            // On retire l'index de la liste à supprimer
            this.indexesToDelete.Remove(button.transform.GetSiblingIndex());
        }
    }


    // Ajout d'un personnage dans la liste et création de son bouton associé
    public void addCharacter (CharacterData character)
    {
        // Création du bouton
        Button newButton = Instantiate(button, Vector3.zero, Quaternion.identity) as Button;
        // Attachement du bouton à l'objet Content
        newButton.transform.SetParent(GameObject.FindWithTag("ListContent").transform, false);
        // Mise à jour du texte du bouton pour correspondre au nom du personnage
        newButton.GetComponentInChildren<CharacterButton>().updateInfos(character.name, character.age, character.strength, character.speed);
        // Redimentionnement de l'objet Content en largeur
        GameObject.FindWithTag("ListContent").GetComponent<RectTransform>().sizeDelta = new Vector2
            (
            25 + (buttonWidth + 25) * dataController.characters.Count, 
            50 + buttonWidth
            );
        // Ajout du bouton à la liste
        this.characters.Add(newButton.gameObject);
    }
}
