﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class AjouterController : AjouterSupprimerHyperclass
{
    // Le DataController
    protected DataController dataController;

    // Caractéristiques à récupérer
    public GameObject texteNom;
    public GameObject texteAge;
    public GameObject texteForce;
    public GameObject texteVitesse;

    public GameObject contentCombos;

    // Prefab combo
    public GameObject prefabCombo;
    // Hauteur d'un prefab combo
    public float hauteurPrefabCombo;
    // Largeur d'un prefab combo
    public float largeurPrefabCombo;



    // Use this for initialization
    void Start()
    {
        // Récupération du DataController
        this.dataController = GameObject.FindWithTag("DataController").GetComponent<DataController>();

    }


    // Fonction appelée lors du clic sur le bouton Annuler
    public void onClickAnnuler()
    {
        // Chargement de la page Liste
        SceneManager.LoadScene("Liste");
    }


    // Fonction appelée lors du clic sur le bouton Valider
    public void onClickValider()
    {
        // Construction du nouveau personnage
        CharacterData newCharacter = new CharacterData();

        // Ajout des caractéristiques, passage des caractérisques numériques vides à 0
        newCharacter.name = texteNom.GetComponent<InputField>().text;

        string strAge = texteAge.GetComponent<InputField>().text;
        if (strAge != "")
        {
            if (strAge[0] != '.')
            {
                newCharacter.age = Convert.ToInt32(strAge);
            }
        }
        else
        {
            newCharacter.age = 0;
        }

        string strStrength = texteForce.GetComponent<InputField>().text;
        if (strStrength != "")
        {
            if (strStrength[0] != '.')
            {
                newCharacter.strength = float.Parse(strStrength);
            }
        }
        else
        {
            newCharacter.strength = 0;
        }

        string strSpeed = texteVitesse.GetComponent<InputField>().text;
        if (strSpeed != "")
        {
            if (strSpeed[0] != '.')
            {
                newCharacter.speed = float.Parse(strSpeed);
            }
        }
        else
        {
            newCharacter.speed = 0;
        }

        // Ajout des combos
        List<ComboData> newCombos = new List<ComboData>();
        for (int i=0 ; i<contentCombos.transform.childCount - 1 ; i++)
        {
            // Création de l'élément
            ComboData newCombo = new ComboData();
            // Ajout du nom
            newCombo.name = contentCombos.transform.GetChild(i).GetComponent<ComboFieldPrefab>().texteNom.GetComponent<InputField>().text;
            // Séparation du champ Mouvements en une liste de string sur ", ", et ajout à l'élément
            newCombo.movements = contentCombos.transform.GetChild(i).GetComponent<ComboFieldPrefab>().texteMouvements.GetComponent<InputField>().text.Split(new string[]{ ", " }, StringSplitOptions.None);
            // Ajout du combo à la liste
            newCombos.Add(newCombo);
        }
        // Insertion des combos dans le nouveau personnage
        newCharacter.combo = newCombos.ToArray();

        // Envoi de l'objet au DataController
        dataController.addCharacter(newCharacter);

        // Sélection du personnage (dernier de la liste)
        dataController.selectedCharacter = dataController.characters.Count - 1;
        // Redirection vers la page du personnage
        SceneManager.LoadScene("CharacterDetail");
    }


    // Fonction appelée lors du clic sur le bouton Ajouter Combo
    public void onClickAjouterCombo()
    {
        // Création du prefab d'un nouveau combo
        GameObject newCombo = Instantiate(prefabCombo, Vector3.zero, Quaternion.identity);
        // Attachement du combo à l'objet Content
        newCombo.transform.SetParent(GameObject.FindWithTag("ListContent").transform, false);
        // Passage à l'avant-dernier index de Content
        newCombo.transform.SetSiblingIndex(newCombo.transform.GetSiblingIndex() - 1);

        // Redimentionnement de l'objet Content en fonction de son contenu
        contentCombos.GetComponent<RectTransform>().sizeDelta = new Vector2
            (
            50 + largeurPrefabCombo,
            80 + (hauteurPrefabCombo + 20) * (contentCombos.transform.childCount - 1)
            );
    }


    // Fonction appelée lors du clic sur le bouton Supprimer Combo
    public override void supprimerCombo(GameObject combo)
    {
        // Redimentionnement de l'objet Content en fonction de son contenu
        contentCombos.GetComponent<RectTransform>().sizeDelta = new Vector2
            (
            50 + largeurPrefabCombo,
            80 + (hauteurPrefabCombo + 20) * (contentCombos.transform.childCount - 2)
            );

        // Destruction du combo
        Destroy(combo.gameObject);
    }
}
