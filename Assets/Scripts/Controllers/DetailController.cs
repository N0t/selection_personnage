﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DetailController : MonoBehaviour
{
    // Le DataController
    protected DataController dataController;

    // Texte des caractéristiques à afficher
    public GameObject texteNom;
    public GameObject texteAge;
    public GameObject texteForce;
    public GameObject texteVitesse;

    // Prefab combo
    public GameObject prefabCombo;
    // Hauteur d'un prefab combo
    public float hauteurPrefabCombo;
    // Largeur d'un prefab combo
    public float largeurPrefabCombo;

    // Personnage sélectionné
    protected CharacterData selectedCharacter;


    // Use this for initialization
    void Start ()
    {
        // Récupération du DataController
        this.dataController = GameObject.FindWithTag("DataController").GetComponent<DataController>();

        // Récupération du personnage
        this.selectedCharacter = dataController.characters[dataController.selectedCharacter];

        // Affichage des caractéristiques
        this.updateInfos();

        // Affichage des combos
        for (int i = 0; i < this.selectedCharacter.combo.Length; i++)
        {
            addCombo(this.selectedCharacter.combo[i]);
        }

        // Redimentionnement de l'objet Content en fonction de son contenu
        GameObject.FindWithTag("ListContent").GetComponent<RectTransform>().sizeDelta = new Vector2
            (
            50 + largeurPrefabCombo,
            30 + (hauteurPrefabCombo + 20) * this.selectedCharacter.combo.Length
            );
    }


    // Fonction appelée lors du clic sur le bouton Retour
    public void onClickRetour()
    {
        // Chargement de la page Liste
        SceneManager.LoadScene("Liste");
    }


    // Fonction appelée lors du clic sur le bouton Modifier
    public void onClickModifier()
    {
        SceneManager.LoadScene("Modifier");
    }


    // Fonction de mise à jour de l'affichage des caractéristiques du personnage
    protected void updateInfos()
    {
        // Vérification de l'existence du nom
        if (this.selectedCharacter.name == null)
        {
            this.texteNom.GetComponent<Text>().text = "";
        }
        else
        {
            this.texteNom.GetComponent<Text>().text = this.selectedCharacter.name;
        }

        // Si les attributs int/float valent 0, on suppose qu'ils sont inexistants dans le JSON
        // Les valeurs des attributs inexistants ne sont pas affichées
        this.texteAge.GetComponent<Text>().text = "Age : ";
        if (this.selectedCharacter.age != 0)
        {
            this.texteAge.GetComponent<Text>().text += this.selectedCharacter.age.ToString();
        }

        this.texteForce.GetComponent<Text>().text = "Force : ";
        if (this.selectedCharacter.strength != 0)
        {
            this.texteForce.GetComponent<Text>().text += this.selectedCharacter.strength.ToString();
        }

        this.texteVitesse.GetComponent<Text>().text = "Vitesse : ";
        if (this.selectedCharacter.speed != 0)
        {
            this.texteVitesse.GetComponent<Text>().text += this.selectedCharacter.speed.ToString();
        }
    }


    // Création et affichage du prefab d'un combo
    public void addCombo(ComboData combo)
    {
        // Création du bouton
        GameObject newCombo = Instantiate(prefabCombo, Vector3.zero, Quaternion.identity);
        // Attachement du combo à l'objet Content
        newCombo.transform.SetParent(GameObject.FindWithTag("ListContent").transform, false);
        // Mise à jour du texte du bouton pour correspondre au nom du personnage
        newCombo.GetComponentInChildren<ComboDetailPrefab>().updateInfos(combo.name, combo.movements);
    }
}
